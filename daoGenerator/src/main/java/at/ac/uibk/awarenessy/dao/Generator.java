/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.dao;


import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

/**
 * Generate dao classes to access db and entities easily.
 */
public class Generator {

    // IMPORTANT! When changing name here, also change name in app.utils.Utils.DB_NAME
    public static final String DB_NAME = "at.ac.uibk.awarenessy.dao";


    public static void main( String[] args ) throws Exception {

        Schema schema = new Schema(9, DB_NAME);

        Entity simpleLocation = addSimpleLocation(schema);

        Entity sms = addSms(schema);

        // SMS has one Location
        Property locationIdSms = sms.addLongProperty("locationId").getProperty();
        sms.addToOne(simpleLocation, locationIdSms);

        Entity call = addCall(schema);

        // to send calls between activities, services, ...
        call.implementsSerializable();
        // Call has one Location
        Property locationIdCall = call.addLongProperty("locationId").getProperty();
        call.addToOne(simpleLocation, locationIdCall);


        new DaoGenerator().generateAll(schema, "app/src-gen/dao/java");

    }


    private static Entity addCall( Schema schema ) {

        Entity call = schema.addEntity("Call");
        call.setHasKeepSections(true);
        call.implementsInterface("Table");

        call.addIdProperty();
        call.addStringProperty("Number").notNull();
        call.addLongProperty("StartTime").notNull().index();
        call.addLongProperty("EndTime");
        call.addBooleanProperty("Outgoing").notNull();
        return call;
    }

    private static Entity addSimpleLocation( Schema schema ) {

        Entity simpleLocation = schema.addEntity("SimpleLocation");
        simpleLocation.setHasKeepSections(true);
        simpleLocation.implementsInterface("Table");

        simpleLocation.addIdProperty();
        simpleLocation.addDoubleProperty("Latitude").notNull().index();
        simpleLocation.addDoubleProperty("Longitude").notNull().index();
        simpleLocation.addStringProperty("Address");
        simpleLocation.addFloatProperty("Accuracy");
        simpleLocation.addLongProperty("Time").notNull().index();
        simpleLocation.addBooleanProperty("IsTrackingPoint");

        return simpleLocation;
    }


    private static Entity addSms( Schema schema ) {

        Entity sms = schema.addEntity("Sms");
        sms.setHasKeepSections(true);
        sms.implementsInterface("Table");

        sms.addIdProperty();
        sms.addStringProperty("Number").notNull().index();
        sms.addStringProperty("Message");
        sms.addLongProperty("Time").notNull().index();
        sms.addBooleanProperty("Sent").notNull();

        return sms;
    }

}
