/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.utils;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;

import at.ac.uibk.awarenessy.app.utils.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.dao.DaoMaster;
import at.ac.uibk.awarenessy.dao.DaoSession;

/**
 * This class contains static utility methods.
 */
public class Utils {

    public static final  DateFormat    DATE_FORMAT    = DateFormat.getDateTimeInstance();
    // these apps will be ignored in the DataUsage module
    public static final  List<String>  IGNORED_APPS   = Arrays.asList("at.samsung.powersleep");
    // if true, IGNORED_APPS will be skipped
    public static final  boolean       IGNORE_APPS    = true;
    // IMPORTANT! When changing name here, also change name in daoGenerator.dao.DB_NAME
    public static final  String        DB_NAME        = "at.ac.uibk.awarenessy.dao";
    public static final  DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");
    public static final  float         USD_TO_EUR     = 1.3840f;
    private static final String        TAG            = Utils.class.getSimpleName();
    private static DaoSession instance;

    // Prevents instantiation.
    private Utils() {
    }

    /**
     * Return the given dollar amount in euro
     *
     * @param dollar amount of dollars
     *
     * @return converted Euro amount with a fix exchange rate set with {@link
     * Utils#USD_TO_EUR}
     */
    public static float UsdToEur(float dollar) {
        return dollar / USD_TO_EUR;
    }

    /**
     * Check if the device has internet connection
     *
     * @return true if the device has a internet connection
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context
                .CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String getFormattedDurationString(long duration, TimeUnit timeUnit) {

        long durationInSeconds = timeUnit.toSeconds(duration);
        return String.format(Locale.US, "%d:%02d:%02d", durationInSeconds / 3600,
                (durationInSeconds % 3600) / 60, (durationInSeconds % 60));
    }

    public static String getElapsedTime(long start, long end) {

        long duration = end - start;

        long s = TimeUnit.NANOSECONDS.toMillis(duration);

        return s + " ms";
    }

    public static synchronized DaoSession getDaoSession(Context context) {

        if (instance == null) {

            DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context,
                    Utils.DB_NAME, null);
            SQLiteDatabase db = helper.getWritableDatabase();

            DaoMaster daoMaster = new DaoMaster(db);

            instance = daoMaster.newSession();

        }

        return instance;
    }


    /**
     * Uses static final constants to detect if the device's platform version is Ice Cream Sandwich
     * (4.0) or later.
     */
    public static boolean isICS() {
        return Build.VERSION.SDK_INT == Build.VERSION_CODES.ICE_CREAM_SANDWICH || Build.VERSION
                                                                                          .SDK_INT == Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1;
    }

    /**
     * Uses static final constants to detect if the device's platform version is JellyBean or
     * later.
     */
    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    /**
     * Uses static final constants to detect if the device's platform version is KitKat or later.
     */
    public static boolean hasKitKat() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }


    /**
     * Converts a SQLite boolean into a Java boolean.
     * <p/>
     * SQLite stores boolean as 0 (false) and 1 (true). If a invalid integer is passed (neither 0
     * nor 1) a IllegalArgumentException is thrown.
     *
     * @param boolInt integer representing a SQLite boolean
     *
     * @return boolean resulting from the integer boolean
     */
    public static boolean SQLiteBoolToBool(int boolInt) {

        if (boolInt == 0) {
            return false;
        } else if (boolInt == 1) {
            return true;
        } else {
            throw new IllegalArgumentException("Boolean Int has to be 0 (false) or 1 (true), " +
                                               "but was " + boolInt);
        }

    }

    /**
     * Converts a Java boolean into a SQLite boolean.
     * <p/>
     * SQLite stores boolean as 0 (false) and 1 (true).
     *
     * @param bool boolean representing either 0 (false) or 1 (true)
     *
     * @return an integer that represents a SQLite boolean
     */
    public static int BoolToSQLiteBool(boolean bool) {
        return bool ? 1 : 0;
    }


    public static boolean isTablet(Context context) {

        return context.getResources().getBoolean(R.bool.isTablet);

    }

    /**
     * Check if the device has phone function (can make calls, sms).
     *
     * @param context Context to get the system service
     *
     * @return true if the device has phone function, else false
     */
    public static boolean hasPhoneFunction(Context context) {

        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context
                .TELEPHONY_SERVICE);

        return manager.getPhoneType() != TelephonyManager.PHONE_TYPE_NONE;
    }

    /**
     * Return a formatted String of the current time.
     *
     * @return String of the current time
     */
    public static String getCurrTimeStamp() {
        return Utils.DATE_FORMAT.format(new Date());
    }

    /**
     * Remove all containing whitespaces from the given string. If the given string is null, return
     * null.
     *
     * @param string String to remove whitespacs
     *
     * @return the given string without whitespaces, or null if the string is null
     */
    public static String removeWhitespaces(String string) {
        if (string != null) {
            return string.replaceAll("\\s", "");
        } else {
            return null;
        }
    }


    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /**
     * Verify that Google Play services is available before making a request.
     *
     * @return true if Google Play services is available, otherwise false
     */
    public static boolean servicesConnected(Activity activity) {

        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {

            // In debug mode, log the status
            Log.d(TAG, "Location Services is available");

            // Continue
            return true;

            // Google Play services was not available for some reason
        } else {

            // Display an error dialog
            GooglePlayServicesUtil.getErrorDialog(resultCode, activity, 0).show();
            return false;
        }
    }
}
