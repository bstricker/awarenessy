/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.checkapps.model;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Class to represent the danger of a application.
 * Consist of a "danger point rating" between 0 (harmless) and 100 (harmfull) and a {@link EnumDanger} category that will be calculated on the basis of the points.
 */
public class Danger implements Parcelable {

    private final int dangerPoints;

    private final EnumDanger danger;

    /**
     * Class Constructor
     * <p/>
     * Create a new Danger object that represents the danger of an app.
     * <p/>
     * Danger will be categorized on the basis of the danger points.
     *
     * @param dangerPoints danger points between 0 and 100
     */
    public Danger( int dangerPoints ) {


        this.dangerPoints = dangerPoints;
        this.danger = ratingToClass(dangerPoints);

    }

    private EnumDanger ratingToClass(int dangerPoints) {
        EnumDanger danger;

        if (dangerPoints > 66) {
            danger = EnumDanger.HARMFUL;
        } else if (dangerPoints > 33) {
            danger = EnumDanger.AVERAGE;
        } else if (dangerPoints > 0) {
            danger = EnumDanger.HARMLESS;
        } else {
            danger = EnumDanger.ABSOLUTE_NO_DANGER;
        }


        return danger;
    }

    public int getDangerPoints() {
        return dangerPoints;
    }

    public EnumDanger getDanger() {
        return danger;
    }

    @Override
    public String toString() {
        return danger + " (" + dangerPoints + ")";
    }

    /*
         *   Parcelable Implementation
         */
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int parcelableFlags) {

        dest.writeInt(dangerPoints);
        dest.writeSerializable(danger);

    }


    public static final Creator<Danger> CREATOR
            = new Creator<Danger>() {
        public Danger createFromParcel(Parcel source) {
            return new Danger(source);
        }

        public Danger[] newArray(int size) {
            return new Danger[size];
        }


    };

    private Danger(Parcel source) {
        dangerPoints = source.readInt();
        danger = (EnumDanger) source.readSerializable();
    }
}
