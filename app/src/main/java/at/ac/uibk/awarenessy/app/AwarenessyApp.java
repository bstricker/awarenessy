package at.ac.uibk.awarenessy.app;

import android.app.Application;

import timber.log.Timber;

/**
 * Application context
 * <p>
 * Initialize Timber logging
 * .
 */

public class AwarenessyApp extends Application {

    @Override
    public void onCreate() {

        super.onCreate();

        if (BuildConfig.DEBUG) {

            Timber.plant(new Timber.DebugTree() {

                @Override
                protected String createStackElementTag(StackTraceElement element) {

                    return super.createStackElementTag(element) + ':' + element.getLineNumber();
                }
            });
        }
        // Optional: plant release Tree

    }
}
