/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.checkapps.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.checkapps.model.MyPermissionInfo;


/**
 * Own ArrayAdapter implementation
 * <p/>
 * Defines a own Layout for a List.
 * Uses apps_permission_list_item.xml as the layout, which displays the Permission-name (User readable) and a Indicator for the danger of the permission.
 * <p/>
 * Created by Benedikt on 12.12.13.
 */
public class PermissionListAdapter extends ArrayAdapter<MyPermissionInfo> {

    private final List<MyPermissionInfo> permissions;

    static class ViewHolder {
        public TextView text;
        public ImageView image;
    }

    public PermissionListAdapter(Activity context, List<MyPermissionInfo> permissions) {
        super(context, R.layout.apps_permission_list_item, permissions);
        this.permissions = permissions;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View appView = convertView;

        if (appView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            appView = inflater.inflate(R.layout.apps_permission_list_item, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) appView.findViewById(R.id.label_list);
            viewHolder.image = (ImageView) appView.findViewById(R.id.danger_indicator);

            appView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) appView.getTag();

        // Permission at the current displayed position
        MyPermissionInfo permission = permissions.get(position);

        holder.text.setText(permission.getName());
        holder.image.setImageResource(permission.getIndicatorRes());


        return appView;
    }


}
