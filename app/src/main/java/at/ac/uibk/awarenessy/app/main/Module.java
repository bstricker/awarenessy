/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.main;

import android.content.Context;
import android.content.SharedPreferences;

import at.ac.uibk.awarenessy.app.utils.Preferences;

/**
 * Enumeration of Modules that can be activated and deactivated in the Privacy Settings.
 * <p/>
 * Each setting will be stored in SharedPreferences {@link at.ac.uibk.awarenessy.app.utils
 * .Preferences.PREF_FLAGS}.
 * <p/>
 * Simplify editing the settings, {@link Module#setChecked
 * (boolean,
 * android.content.Context)} and {@link Module#isChecked(android.content.Context)} are provided.
 */
public enum Module {
    SMS(Preferences.PREF_FLAGS.MONITOR_SMS),
    POSITION(Preferences.PREF_FLAGS.TRACK_POSITION),
    CALL(Preferences.PREF_FLAGS.MONITOR_CALLS);

    private String prefName;

    private Module( String prefName ) {
        this.prefName = prefName;
    }

    /**
     * Set the Module to activated or deactivated.
     *
     * @param checked true if the Module should turned on, else false
     * @param context to get the SharedPreferences
     */
    public void setChecked( boolean checked, Context context ) {

        SharedPreferences.Editor editor = context.getSharedPreferences(Preferences.PREF_FLAGS
                                                                               .PREF_NAME,
                                                                       Context.MODE_PRIVATE
        ).edit();

        editor.putBoolean(getPrefName(), checked);


        editor.commit();

    }

    /**
     * Check if the Module is turned off or on.
     *
     * @param context to get the SharedPreferences
     *
     * @return true if the Module is activated, else false
     */
    public boolean isChecked( Context context ) {

        SharedPreferences pref = context.getSharedPreferences(Preferences.PREF_FLAGS.PREF_NAME,
                                                              Context.MODE_PRIVATE);

        return pref.getBoolean(getPrefName(), false);


    }

    /**
     * Returns the name of the module settings.
     * @return the name of the module setting
     */
    public String getPrefName() {
        return prefName;
    }

    @Override
    public String toString() {
        return prefName;
    }

}