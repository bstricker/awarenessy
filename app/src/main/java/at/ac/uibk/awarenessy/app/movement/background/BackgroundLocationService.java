/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.movement.background;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.List;
import java.util.concurrent.TimeUnit;

import at.ac.uibk.awarenessy.app.BuildConfig;
import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.movement.LocationUtils;
import at.ac.uibk.awarenessy.app.utils.LogFile;
import at.ac.uibk.awarenessy.app.utils.Preferences;
import at.ac.uibk.awarenessy.app.utils.Utils;
import at.ac.uibk.awarenessy.dao.DaoSession;
import at.ac.uibk.awarenessy.dao.SimpleLocation;
import at.ac.uibk.awarenessy.dao.SimpleLocationDao;
import timber.log.Timber;

/**
 * BackgroundLocationService used for tracking user positions in the background. The interval on
 * which updates should be received will be set onCreate and each time a local broadcast with new
 * intervals will be received. This broadcast will be send from the {@link
 * ActivityRecognitionIntentService} each time the user changes its activity.
 * <p/>
 * Taken and adapted from <a href="https://gist.github.com/blackcj/20efe2ac885c7297a676">
 * https://gist.github.com/blackcj/20efe2ac885c7297a676</a>
 */
public class BackgroundLocationService extends Service implements GoogleApiClient
        .ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public static boolean SERVICE_RUNNING = false;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;


    // dao to store new location updates
    private SimpleLocationDao mLocationDao;

    private SimpleLocation mLastLocation;


    private int getPreviousActivity() {

        return getSharedPreferences(Preferences.APP_DATA.PREF_NAME, Context.MODE_PRIVATE).getInt
                (Preferences.APP_DATA.KEY_PREVIOUS_ACTIVITY_TYPE, DetectedActivity.UNKNOWN);
    }

    /**
     * Set up db to store new locations
     */
    private void setUpDb() {

        DaoSession daoSession = Utils.getDaoSession(this);
        mLocationDao = daoSession.getSimpleLocationDao();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Timber.d("%s onStart. Action: %s", this.getClass().getSimpleName(), intent.getAction());

        Timber.d("Service running: %b", SERVICE_RUNNING);

        String request = intent.getAction();

         /*
         * If the incoming Intent was a request to start location updates
         */
        if ((TextUtils.equals(request, LocationUtils.ACTION_START_LOCATION_UPDATES))) {

            int activityType = intent.getIntExtra(LocationUtils.EXTRA_ACTIVITY_TYPE,
                    getPreviousActivity());

            // if no moving activity is passed and the service isn't running, stop self
            if (!ActivityRecognitionIntentService.isMoving(activityType) && !SERVICE_RUNNING) {
                Timber.d("Not moving. Stop self.");
                LogFile.getInstance(this).log(LogFile.LOG_FILE_BACKGROUND_LOCATION_SERVICE,
                        getString(R.string.log_not_moving_stop));
                stopSelf();
                return START_STICKY;
            }

            long updateInterval = LocationUtils.getNewUpdateIntervalInMilliseconds(activityType);


            // Create the LocationRequest object
            mLocationRequest = new LocationRequest();
            // Use high accuracy
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            // Smallest distance between two updates
            //            mLocationRequest.setSmallestDisplacement(LocationUtils.MIN_DISTANCE);
            // Set the update interval
            mLocationRequest.setInterval(updateInterval);
            // Set the fastest update interval, as fast as the standard interval
            mLocationRequest.setFastestInterval(updateInterval);


            // use float to get a more accurate log
            float intervalInSeconds = TimeUnit.SECONDS.convert(mLocationRequest.getInterval(),
                    TimeUnit.MILLISECONDS);
            float fastIntervalInSeconds = TimeUnit.SECONDS.convert(mLocationRequest
                            .getFastestInterval(),
                    TimeUnit.MILLISECONDS
            );


            Timber.d("Update interval: " + intervalInSeconds);
            Timber.d("Fast update interval: " + fastIntervalInSeconds);
            LogFile.getInstance(this).log(LogFile.LOG_FILE_BACKGROUND_LOCATION_SERVICE,
                    getString(R.string
                                    .log_background_location_started,
                            intervalInSeconds
                    )
            );


            setUpLocationClientIfNeeded();


            mGoogleApiClient.connect();


            SERVICE_RUNNING = true;


        } else if (TextUtils.equals(request, LocationUtils.ACTION_STOP_LOCATION_UPDATES)) {

            LogFile.getInstance(this).log(LogFile.LOG_FILE_BACKGROUND_LOCATION_SERVICE,
                    getString(R.string.log_stop_location_updates));
            stopSelf();

        } else {
            Timber.d("Unknown Action " + request + " Stopping self.");
            LogFile.getInstance(this).log(LogFile.LOG_FILE_BACKGROUND_LOCATION_SERVICE,
                    getString(R.string.log_unkown_action_stop, request));
            stopSelf();
        }

        return START_REDELIVER_INTENT;
    }

    /*
    * Create a new location client, using the enclosing class to
    * handle callbacks.
    */

    private void setUpLocationClientIfNeeded() {

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }


    // Define the callback method that receives location updates
    @Override
    public void onLocationChanged(Location location) {

        setUpDb();

        Timber.d("onLocationChanged: %s", location.toString());

        // insert only if the new location is more accurate, or it has a new position and is
        // enough distant from the last
        if (checkDistance(location) && checkTime(location) && checkAccuracy(location)) {
            insertNewLocation(location);
        }


    }


    /**
     * Check if the location has a specific accuracy.
     * <p/>
     * Accuracy is specified with {@link at.ac.uibk.awarenessy.app.movement
     * .LocationUtils#MIN_ACCURACY}. If this location does not have an accuracy, then 0.0 is
     * returned, so it isn't useful.
     *
     * @param location the location which accuracy should be checked
     * @return true if the given location is accurate enough, else false
     */
    private boolean checkAccuracy(Location location) {

        if (location.getAccuracy() < LocationUtils.MIN_ACCURACY && location.getAccuracy() != 0.0) {
            return true;
        } else {
            Timber.d("Not inserted, not accurate enough");
            return false;
        }
    }


    /**
     * Check if the distance between the given location and the last stored location is greater
     * than
     * {@link at.ac.uibk.awarenessy.app.movement.LocationUtils#MIN_DISTANCE} but in the vicinity of
     * the last location. Therefore the speed that is needed to travel this distance is calculated.
     * This speed has to be plausible and therefore not greater than
     * {@link at.ac.uibk.awarenessy.app.movement.LocationUtils#MAX_SPEED}.
     *
     * @param location Location to calculate the distance between this and the last stored tracking
     *                 point
     * @return true if the distance is greater than {@link at.ac.uibk.awarenessy.app
     * .movement.LocationUtils#MIN_DISTANCE} but not too great, else false
     */
    private boolean checkDistance(Location location) {

        SimpleLocation lastLocation = getLastLocation();

        if (lastLocation != null) {


            float distance = LocationUtils.distanceBetween(lastLocation.getLatitude(),
                    lastLocation.getLongitude(),
                    location.getLatitude(),
                    location.getLongitude());

            Timber.d("Distance: " + distance);

            boolean enoughDistance = distance > LocationUtils.MIN_DISTANCE;

            // distance is too short => no insert (avoid too much similar locations)
            if (!enoughDistance) {
                Timber.d("Not inserted, too close to last location");
                return false;
            }

            long lastTime = lastLocation.getTime();
            long thisTime = location.getTime();

            long deltaSec = TimeUnit.MILLISECONDS.toSeconds(thisTime - lastTime);


            float mPerS = distance / deltaSec;

            boolean plausibleSpeed = mPerS < LocationUtils.MAX_SPEED;

            Timber.d("DeltaSec " + deltaSec + "  m/s " + mPerS + "  plausible " + plausibleSpeed);

            // user moves with an incredible speed => not possible (wrong gps fix)
            if (!plausibleSpeed) {
                Timber.d("Not inserted, implausible speed: more than " + mPerS + " m/s");
                return false;
            }

            // distance and speed are ok so update travelled distance (just sum up each point)
            addTravelledDistance(distance);
            return true;

        } else {
            // if location is the very first location
            return true;
        }

    }

    /**
     * Check if the insert of the last location is long enough ago (Fastest Interval). A faster
     * interval could happen if the BackgroundLocationService has been restarted short after the
     * last insert so it will insert a location again.
     *
     * @param location Location to calculate the time between this and the last stored tracking
     *                 point
     * @return true if the last location is insert more than
     * {@link com.google.android.gms.location.LocationRequest#getFastestInterval()}
     * ago, else false
     */
    private boolean checkTime(Location location) {

        SimpleLocation lastLocation = getLastLocation();

        if (lastLocation != null) {

            long delta = location.getTime() - lastLocation.getTime();

            // insert of the last location is not old enough
            if (delta < mLocationRequest.getFastestInterval()) {
                Timber.d("Not inserted, last location is too up-to-date");
                return false;
            }

        }

        return true;

    }

    /**
     * Add the given distance to the stored travelled distance. The travelled distance will be
     * displayed in {@link at.ac.uibk.awarenessy.app.overview.OverviewFragment}.
     *
     * @param distance the distance to add to the previous covered distance
     */
    private void addTravelledDistance(float distance) {

        SharedPreferences pref = getSharedPreferences(Preferences
                        .APP_DATA.PREF_NAME,
                MODE_PRIVATE
        );

        float oldDistance = pref.getFloat(Preferences.APP_DATA.KEY_DISTANCE_TRAVELLED, 0);
        oldDistance += distance;

        pref.edit().putFloat(Preferences.APP_DATA.KEY_DISTANCE_TRAVELLED,
                oldDistance).commit();
    }

    /**
     * Return the last stored location.
     *
     * @return the last stored location, or null if there is no location stored yet.
     */
    private SimpleLocation getLastLocation() {

        // we haven't cached the last location, so get it from the database
        if (mLastLocation == null) {

            // location with the highest time ( = newest location)
            List<SimpleLocation> list = mLocationDao.queryBuilder().orderDesc(SimpleLocationDao
                    .Properties
                    .Time)
                    .limit(1).list();

            // got our location, cache and return it
            if (list.size() == 1) {
                mLastLocation = list.get(0);
                Timber.d("Last Tracking point: " + mLastLocation.toString());
            }
        }

        /*
         *  either a cached location or a location from the database, or ,if both failed, null
          */
        return mLastLocation;
    }

    /**
     * Insert the given location into the database
     *
     * @param location the new location to insert
     */
    private void insertNewLocation(Location location) {

        // convert it into the dao object
        SimpleLocation simpleLocation = new SimpleLocation(location);

        // the location comes from the locationservice
        simpleLocation.setIsTrackingPoint(true);

        // calculate Address
        String address = LocationUtils.calcLocationAddress(this, simpleLocation);
        simpleLocation.setAddress(address);
        Timber.d("Address: " + simpleLocation.getAddress());

        mLocationDao.insert(simpleLocation);


        // cache last location
        mLastLocation = simpleLocation;


        // log everything
        final String position = LocationUtils.getLatLngString(this, simpleLocation.getLatLng());
        final String string = getString(R.string.location_point, simpleLocation.getId(), position,
                simpleLocation.getAccuracy(), simpleLocation.getAddress());
        Timber.d(string);
        LogFile.getInstance(this).log(LogFile.LOG_FILE_BACKGROUND_LOCATION_SERVICE,
                getString(R.string.log_new_location, string));

        if (BuildConfig.DEBUG) {
            Toast.makeText(this, string, Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }


    @Override
    public void onDestroy() {
        // Turn off the request flag
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
            // Destroy the current location client
            mGoogleApiClient = null;
        }

        // Display the connection status
        Timber.d("Destroy");

        SERVICE_RUNNING = false;
        super.onDestroy();
    }

    /*
    * Called by Location Services when the request to connect the
    * client finishes successfully. At this point, you can
    * request the current location or start periodic updates
    */
    @Override
    public void onConnected(Bundle bundle) {

        // Request location updates using static settings
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Timber.d("Connected");
    }

    @Override
    public void onConnectionSuspended(int i) {

        Timber.d("onConnectionSuspended %d", i);
    }

    /*
    * Called by Location Services if the attempt to
    * Location Services fails.
    */
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {


        Timber.d("Connection failed. Stopping self.");
        stopSelf();
    }
}