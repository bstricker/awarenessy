/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.utils;

import at.ac.uibk.awarenessy.app.BuildConfig;

/**
 * Created by Benedikt on 26.05.2014.
 */
public class Log {

    private static boolean LOG = BuildConfig.DEBUG;
//    private static boolean LOG = true;

    /**
     * Send a {@link android.util.Log#v(String, String)} log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     *            the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void v( String tag, String msg ) {
        if ( LOG ) { android.util.Log.v(tag, msg); }
    }

    /**
     * Send a {@link android.util.Log#i(String, String)} log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     *            the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void i( String tag, String msg ) {
        if ( LOG ) { android.util.Log.i(tag, msg); }
    }

    /**
     * Send a {@link android.util.Log#d(String, String)} log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     *            the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void d( String tag, String msg ) {
        if ( LOG ) { android.util.Log.d(tag, msg); }
    }

    /**
     * Send a {@link android.util.Log#w(String, String)} log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     *            the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void w( String tag, String msg ) {
        if ( LOG ) { android.util.Log.w(tag, msg); }
    }

    /**
     * Send a {@link android.util.Log#e(String, String)} log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     *            the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void e( String tag, String msg ) {
        if ( LOG ) { android.util.Log.e(tag, msg); }
    }

    /**
     * Send a {@link android.util.Log#e(String, String)} log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     *            the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr  An exception to log
     */
    public static void e( String tag, String msg, Throwable tr ) {
        if ( LOG ) { android.util.Log.e(tag, msg, tr); }
    }


    /**
     * Send a {@link android.util.Log#wtf(String, String)} log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     *            the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void wtf( String tag, String msg ) {
        if ( LOG ) { android.util.Log.wtf(tag, msg); }
    }

}
