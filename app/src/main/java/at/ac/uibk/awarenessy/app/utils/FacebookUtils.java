/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.utils;

import android.app.Activity;
import at.ac.uibk.awarenessy.app.utils.Log;

import com.facebook.Session;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

/**
 * Helper class to simplify common Facebook routines.
 */
public class FacebookUtils {

    /*
      * Revenue First Quarter 2014
      *from http://investor.fb.com/releasedetail.cfm?ReleaseID=842071
      */
    private static final float AD_REVENUE_BILLIONS    = 2.27f;
    private static final float MOBILE_AD_REVENUE_PERC = 0.59f;

    private static final int   DAYS_IN_Q1       = 31 + 28 + 31;
    public static final  float DAILY_AD_REVENUE = AD_REVENUE_BILLIONS / DAYS_IN_Q1;


    // https://www.facebook.com/photo.php?fbid=10151908376831729&set=a.10151908376636729
    // .1073741825.20531316728&type=1&theater
    public static final float DAILY_LIKES_BILLIONS = 4.5f;

    // http://socialbarrel.com/facebook-photo-library-now-250-billion-user-photos/53315/
    public static final float DAILY_ITEMS_SHARED_BILLIONS = 4.75f;

    private static final float DAILY_LIKES_AND_ITEM_BILLIONS = DAILY_LIKES_BILLIONS +
            DAILY_ITEMS_SHARED_BILLIONS;

    // Value of a Post or Like
    public static final float FB_POST_LIKE_VALUE = DAILY_AD_REVENUE /
            DAILY_LIKES_AND_ITEM_BILLIONS;

    public static final  SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat
            ("yyyy-MM-dd'T'HH:mm:ssZ");
    private static final String           TAG                = FacebookUtils.class.getSimpleName();

    public static final  List<String>     PERMS              = Arrays.asList("read_stream", "user_likes", "user_status");

    /**
     * Check if the user is logged in.
     * <p/>
     * Facebook doesn't provide a convenient method to check this, so we have to check if there
     * exists a current {@link com.facebook.Session} and if it's open.
     * <p/>
     * An unwanted side effect is that if the app tries to log in (after the start of the app,
     * which
     * takes about 200ms) this method returns false, although the user is actually logged in (resp.
     * will be logged in shortly).
     * <p/>
     * So try to check this method periodically (e.g. each 100ms) up to a desired maximum (e.g. 5
     * tries).
     *
     * @return true if the user is logged in, else false
     */
    public static boolean isLoggedIn() {
        Session session = Session.getActiveSession();
        return session != null && session.isOpened();
    }

    public static void requestPermission( Activity activity, String permission ) {
        Session session = Session.getActiveSession();

        if ( isLoggedIn() ) {
            session.requestNewReadPermissions(new Session.NewPermissionsRequest(activity,
                                                                                permission));
        }
    }

    public static boolean hasPermission( String permName ) {
        Session session = Session.getActiveSession();
        List<String> permissions = session.getPermissions();

        Log.d(TAG, "Permissions: " + Arrays.toString(permissions.toArray()));


        return session != null && permissions.contains(permName);
    }

}
