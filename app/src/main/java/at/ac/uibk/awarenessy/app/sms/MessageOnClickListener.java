/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.sms;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.dao.Sms;
import at.ac.uibk.awarenessy.app.utils.Utils;

/**
 * Listener that opens a Dialog with the details of the messages.
 */
public class MessageOnClickListener implements View.OnClickListener {

    private final Context mContext;
    private final Sms mMessage;

    public MessageOnClickListener(Context context, Sms message) {

        mContext = context;
        mMessage = message;
    }


    @Override
    public void onClick(View v) {

        // Build a new Dialog to show details of the clicked message.
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        // Use a custom layout, so get LayoutInflater and inflate the view. Dialog titel and possible Buttons are uneffected.
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.sms_message_dialog, null);
        TextView message = (TextView) view.findViewById(android.R.id.message);
        TextView time = (TextView) view.findViewById(R.id.time);

        // set sent/received icon
        if (mMessage.getSent()) {
            builder.setIcon(R.drawable.ic_sms_sent);
        } else {
            builder.setIcon(R.drawable.ic_sms_received);
        }

        builder.setView(view);

        String name = SmsInterface.getName(mMessage.getNumber(), mContext);

        builder.setTitle(name);
        message.setText(mMessage.getMessage());
        time.setText(Utils.DATE_FORMAT.format(mMessage.getTime()));

        builder.setCancelable(true);
        builder.setPositiveButton(android.R.string.ok, null);

        Dialog dialog = builder.show();
        dialog.setCanceledOnTouchOutside(true);

    }
}
