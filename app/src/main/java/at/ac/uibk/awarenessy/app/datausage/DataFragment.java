/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.datausage;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.TrafficStats;
import android.os.Bundle;
import at.ac.uibk.awarenessy.app.utils.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.main.ModuleFragment;
import at.ac.uibk.awarenessy.app.phoneprovider.adapter.Data;
import at.ac.uibk.awarenessy.app.phoneprovider.adapter.UsedDataAdapter;
import at.ac.uibk.awarenessy.app.phoneprovider.adapter.TrafficData;
import at.ac.uibk.awarenessy.app.utils.Preferences;
import at.ac.uibk.awarenessy.app.utils.Utils;

/**
 * Fragment that shows the Data Traffic for each App that causes some up or download traffic since
 * reboot. Shows at the bottom the total up and download traffic since reboot.
 */
public class DataFragment extends ModuleFragment {

    public static final  String FRAGMENT_TAG = DataFragment.class.getName();
    private static final String TAG          = DataFragment.class.getSimpleName();
    private ListView mListView;
    private TextView mTotalUploadView;
    private TextView mTotalDownloadView;

    private Data mTotalDownloadData;
    private Data mTotalUploadData;


    @Override
    /**
     * Show an info dialog, what the usage and purpose of this Fragment/Module is.
     * <p/>
     * Consists of a simple title, string and an ok button.
     */
    public void showInfoDialog() {

        // Build a new Dialog to tell the user to restart the phone
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setIcon(R.drawable.ic_dialog_info);

        builder.setTitle(R.string.data_usage);

        builder.setMessage(R.string.data_usage_dialog_info);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick( DialogInterface dialog, int which ) {
                // Restore preferences
                SharedPreferences settings = getActivity().getSharedPreferences(Preferences
                                                                                        .PREF_FLAGS.PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(Preferences.PREF_FLAGS.SHOW_DATA_USAGE_INFO, false);


                // Commit the edits!
                editor.commit();
            }
        });

        builder.show();
    }


    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {

        View rootView = inflater.inflate(R.layout.data_fragment, container, false);

        mListView = ( ListView ) rootView.findViewById(android.R.id.list);
        mTotalUploadView = ( TextView ) rootView.findViewById(R.id.total_upload);
        mTotalDownloadView = ( TextView ) rootView.findViewById(R.id.total_download);

        mListView.setEmptyView(rootView.findViewById(android.R.id.empty));

        SharedPreferences settings = getActivity().getSharedPreferences(Preferences
                                                                                .PREF_FLAGS
                                                                                .PREF_NAME,
                                                                        Context.MODE_PRIVATE
        );
        boolean showInfo = settings.getBoolean(Preferences.PREF_FLAGS.SHOW_DATA_USAGE_INFO, true);

        if ( showInfo ) {
            showInfoDialog();
        }

        return rootView;
    }

    @Override
    public void onStart() {

        super.onStart();


        // first try to get data direct from the system
        List<TrafficData> dataList = calcDataProcUidStat();

        Log.d(TAG, "calcDataProcUidStat: " + dataList.size() + " Traffic Dates");

        // else use data from TrafficStats Class
        if ( dataList.isEmpty() ) {
            Log.d(TAG, "calcDataProcUidStat found nothing, use calcDataTrafficStats");
            dataList = calcDataTrafficStats();
            Log.d(TAG, "calcDataTrafficStats: " + dataList.size() + " Traffic Dates");

        }

        Collections.sort(dataList, TrafficData.ALL_DATA_COMPARATOR);

        // set total data
        mTotalUploadView.setText(mTotalUploadData.toString());
        mTotalDownloadView.setText(mTotalDownloadData.toString());

        UsedDataAdapter adapter = new UsedDataAdapter(getActivity(), dataList);

        mListView.setAdapter(adapter);


    }

    /**
     * Return the traffic data for each App since the last reboot. Uses {@link
     * at.ac.uibk.awarenessy.app.phoneprovider.adapter.TrafficData} to read traffic. On some
     * devices this returns only the traffic from the current app.
     * <p/>
     * Retrieving (and also storing) traffic data in Android is based on the apps uid. This has
     * some
     * consequences for the understanding of the traffic data:
     * <ul><li>Some traffic show up in other apps/uid (e.g. watching YouTube is added to media's
     * uid</li>
     * <li>Some </li></ul>
     *
     * @return List of TrafficData that contains the traffic for every app since reboot
     */
    private List<TrafficData> calcDataTrafficStats() {

        List<ApplicationInfo> installedApplications = getActivity().getPackageManager()
                .getInstalledApplications(0);

        Map<Integer, TrafficData> dataMap = new HashMap<Integer,
                TrafficData>(installedApplications.size());
        PackageManager packageManager = getActivity().getPackageManager();


        long totalBytesUpload = 0;
        long totalBytesDownload = 0;


        for ( ApplicationInfo app : installedApplications ) {

            int uid = app.uid;

            String name = packageManager.getNameForUid(uid);


            long rcvBytes = TrafficStats.getUidRxBytes(uid);
            long sendBytes = TrafficStats.getUidTxBytes(uid);

            if ( rcvBytes != 0 || sendBytes != 0 ) {

                // sum up for every app
                totalBytesUpload += sendBytes;
                totalBytesDownload += rcvBytes;

                TrafficData data = new TrafficData(uid, name,
                                                   new Data(sendBytes), new Data(rcvBytes)
                );


                dataMap.put(uid, data);

            }
        }

        mTotalUploadData = new Data(totalBytesUpload);
        mTotalDownloadData = new Data(totalBytesDownload);
        return new ArrayList<TrafficData>(dataMap.values());
    }


    /**
     * Return the traffic data for each App since the last reboot. Reads traffic from the files in
     * /proc/uid_stat/. On some devices this folder doesn't exist, so an empty list is returned.
     * <p/>
     * Retrieving (and also storing) traffic data in Android is based on the apps uid. This has
     * some
     * consequences for the understanding of the traffic data:
     * <ul><li>Some traffic show up in other apps/uid (e.g. watching YouTube is added to media's
     * uid</li>
     * <li>Some </li></ul>
     *
     * @return List of TrafficData that contains the traffic for every app since reboot
     */
    private List<TrafficData> calcDataProcUidStat() {

        List<TrafficData> dataList = new ArrayList<TrafficData>();
        PackageManager packageManager = getActivity().getPackageManager();

        long totalBytesUpload = 0;
        long totalBytesDownload = 0;

        // in this folder each app that caused traffic since reboot has a own folder,
        // named after it's uid, containing the a file for send and received bytes
        String pathToUidFolder = "/proc/uid_stat/";

        File dir = new File(pathToUidFolder);

        // get all uids
        String[] children = dir.list();

        if ( children != null ) {

            // each child is a folder which contains two files containing the send and received
            // bytes
            for ( String child : children ) {
                int uid = Integer.parseInt(child);

                String name = packageManager.getNameForUid(uid);

                File uidFileDir = new File(pathToUidFolder + child);

                File uidDownload = new File(uidFileDir, "tcp_rcv");
                File uidUpload = new File(uidFileDir, "tcp_snd");


                // each file contains only of a number describing the bytes send/received
                long sendBytes = getBytesOfFile(uidUpload);
                long rcvBytes = getBytesOfFile(uidDownload);


                Log.d(TAG, "UID: " + uid + " name: " + name + " rx: " + (rcvBytes != TrafficStats
                        .UNSUPPORTED ? rcvBytes :
                        "UNSUPPORTED") + " tx: " + (sendBytes != TrafficStats.UNSUPPORTED ?
                        sendBytes :
                        "UNSUPPORTED"));

                // some apps didn't work like expected, exclude them
                if ( name != null && Utils.IGNORED_APPS.contains(name) ) {
                    Log.d(TAG, "Non-working App found (wrong data usage): " + name + "\t will be " +
                            "ignored");
                    continue;
                }


                // sum up for every app
                totalBytesUpload += sendBytes;
                totalBytesDownload += rcvBytes;

                TrafficData data = new TrafficData(uid, name,
                                                   new Data(sendBytes), new Data(rcvBytes)
                );


                dataList.add(data);


            }
        }


        mTotalUploadData = new Data(totalBytesUpload);
        mTotalDownloadData = new Data(totalBytesDownload);
        return dataList;
    }

    /**
     * Read bytes from given file.
     * These files only contain a single number and a newline which represents the byte for this
     * uid
     * (filename).
     *
     * @param file the file of which the bytes should be read
     *
     * @return the bytes of the uid, from which this file is, has send or received
     */
    private long getBytesOfFile( File file ) {

        BufferedReader br = null;
        String line = null;
        try {
            br = new BufferedReader(new FileReader(file));

            // this line contains the bytes
            line = br.readLine();

            if ( line != null ) {
                return Long.parseLong(line);

            }

        } catch ( IOException e ) {
            Log.e(TAG, "Couldn't read from file '" + file.toString() + "'");
        } catch ( NumberFormatException e ) {
            Log.e(TAG, "Error parsing bytes " + line + ". Check correct file name: " + file
                    .toString());
        } finally {
            if ( br != null ) {
                try {
                    br.close();
                } catch ( IOException e ) {
                    Log.e(TAG, "Error closing BufferedReader");
                    e.printStackTrace();
                }
            }
        }

        return -1;
    }


}
