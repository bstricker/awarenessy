/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.main.model;

import android.support.annotation.DrawableRes;

/**
 * This class represents a menu item. Contain a icon and the name.
 * <p/>
 * Provides oppurtunity to show a counter on the right side.
 */
public class NavDrawerItem {

    private final String title;
    @DrawableRes private final int    icon;
    private boolean enabled = true;

    /**
     * Constructor
     * <p/>
     * Create a navigation drawer item with the specified title and icon. If item should be
     * disabled (e.g. device didn't support
     * some features like telephone) this has to be set here.
     *
     * @param title     Name of the menu item
     * @param icon      Icon of the menu item
     * @param isEnabled true if menu should be enabled and clickable, false otherwise
     */
    public NavDrawerItem(String title, @DrawableRes int icon, boolean isEnabled) {
        this.title = title;
        this.icon = icon;
        this.enabled = isEnabled;
    }

    public String getTitle() {
        return this.title;
    }

    public int getIcon() {
        return this.icon;
    }


    public boolean isEnabled() {
        return enabled;
    }


}
