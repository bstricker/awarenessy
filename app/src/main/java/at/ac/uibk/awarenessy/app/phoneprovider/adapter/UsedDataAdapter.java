/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.phoneprovider.adapter;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import at.ac.uibk.awarenessy.app.utils.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import at.ac.uibk.awarenessy.app.R;

/**
 * ArrayAdapter to display used data per app from a {@link TrafficData} item. Displays the package icon, package name, up and
 * download for a given package.
 *
 * @see {@link TrafficData}
 */
public class UsedDataAdapter extends ArrayAdapter<TrafficData> {

    private static final String TAG = UsedDataAdapter.class.getSimpleName();
    private final Context        mContext;
    private final PackageManager mPackageManager;

    public UsedDataAdapter(Context context, List<TrafficData> dataList) {

        super(context, R.layout.used_data_list_item, dataList);
        mContext = context;
        mPackageManager = context.getPackageManager();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View dataView = convertView;

        TrafficData data = getItem(position);

        Data sendData = data.getSentData();
        Data rcvData = data.getRcvData();
        String name = data.getName();

        String label = name;
        try {
            ApplicationInfo applicationInfo = mPackageManager.getApplicationInfo(name, 0);
            label = String.valueOf(applicationInfo.loadLabel(mPackageManager));

        } catch (PackageManager.NameNotFoundException e) {
//            Log.d(TAG, "Couldn't found ApplicationInfo for " + name + "\t" + e
//                    .getLocalizedMessage());
        }

        if (dataView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context
                    .LAYOUT_INFLATER_SERVICE);
            dataView = inflater.inflate(R.layout.used_data_list_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.nameView = (TextView) dataView.findViewById(R.id.name);
            viewHolder.appIcon = (ImageView) dataView.findViewById(R.id.appIcon);
            viewHolder.sendDataView = (TextView) dataView.findViewById(R.id.sentData);
            viewHolder.rcvDataView = (TextView) dataView.findViewById(R.id.rcvData);
            dataView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) dataView.getTag();

        try {
            holder.appIcon.setImageDrawable(mPackageManager.getApplicationIcon(name));
        } catch (PackageManager.NameNotFoundException e) {
            Log.w(TAG, "Couldn't get ApplicationInfo for " + name + "\t" + e.getLocalizedMessage());
            holder.appIcon.setImageDrawable(mPackageManager.getDefaultActivityIcon());
        }


        holder.nameView.setText(label);
        holder.sendDataView.setText(sendData.toString());
        holder.rcvDataView.setText(rcvData.toString());


        // Returns the item layout view
        return dataView;


    }

    static class ViewHolder {

        ImageView appIcon;
        TextView  nameView;
        TextView  rcvDataView;
        TextView  sendDataView;
    }


}
