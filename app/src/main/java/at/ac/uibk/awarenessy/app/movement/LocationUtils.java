/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.movement;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import at.ac.uibk.awarenessy.app.utils.Log;

import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import at.ac.uibk.awarenessy.app.R;
import at.ac.uibk.awarenessy.app.utils.Utils;
import at.ac.uibk.awarenessy.dao.DaoSession;
import at.ac.uibk.awarenessy.dao.SimpleLocation;
import at.ac.uibk.awarenessy.dao.SimpleLocationDao;


/**
 * Defines app-wide constants and utilities
 */
public final class LocationUtils {

    // ------------------------------ FIELDS ------------------------------

    public static final String ACTION_START_GPS_TRACKING = LocationUtils.class.getName().concat
            (".ACTION_START_GPS_TRACKING");
    public static final String ACTION_STOP_GPS_TRACKING  = LocationUtils.class.getName().concat
            (".ACTION_STOP_GPS_TRACKING");

    public static final String ACTION_START_LOCATION_UPDATES = LocationUtils.class.getName()
            .concat(".ACTION_START_LOCATION_UPDATES");
    public static final String ACTION_STOP_LOCATION_UPDATES  = LocationUtils.class.getName()
            .concat(".ACTION_STOP_LOCATION_UPDATES");
    public static final String ACTION_PLAY_SERVICE_ERROR     = LocationUtils.class.getName()
            .concat(".ACTION_PLAY_SERICE_ERROR");

    public static final String EXTRA_ERROR_CODE    = LocationUtils.class.getName().concat
            (".EXTRA_ERROR_CODE");
    public static final String EXTRA_ACTIVITY_TYPE = LocationUtils.class.getName().concat
            (".EXTRA_ACTIVITY_TYPE");

    /*
     * Define a request code to send to Google Play services
     * This code is returned in Activity.onActivityResult
     */
    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    // min distance between two locations
    public final static int MIN_DISTANCE = 200;

    // max speed to travel between two locations
    public static final float MAX_SPEED = 360;

    // min accuracy
    public final static int MIN_ACCURACY = 1500;

    // Constants that define the activity detection interval
    private static final int  DETECTION_INTERVAL_SECONDS      = 60 * 10;
    public static final long DETECTION_INTERVAL_MILLISECONDS = TimeUnit.SECONDS.toMillis
            (DETECTION_INTERVAL_SECONDS);


    // Create an empty string for initializing strings
    private static final String EMPTY_STRING = "";

    private static final String TAG = LocationUtils
            .class.getSimpleName();

    private static final int STANDARD_UPDATE_INTERVAL_VEHICLE_IN_SECONDS = 60;
    private static final int STANDARD_UPDATE_INTERVAL_BICYCLE_IN_SECONDS = 150;
    private static final int STANDARD_UPDATE_INTERVAL_WALKING_IN_SECONDS = 300;


    // -------------------------- STATIC METHODS --------------------------

    public static float distanceBetween(SimpleLocation startLocation, SimpleLocation endLocation) {

        return distanceBetween(startLocation.getLatitude(), startLocation.getLongitude(),
                endLocation.getLatitude(), endLocation.getLongitude());

    }

    public static float distanceBetween(LatLng startLocation, LatLng endLocation) {

        return distanceBetween(startLocation.latitude, startLocation.longitude,
                endLocation.latitude, endLocation.longitude);

    }

    public static float distanceBetween(double startLatitude, double startLongitude,
                                        double endLatitude, double endLongitude) {
        float[] result = new float[1];
        Location.distanceBetween(startLatitude, startLongitude,
                endLatitude, endLongitude, result);

        return result[0];
    }

    /**
     * Calculate the standard update interval in milliseconds for the given activity type.
     *
     * @param activityType the type of the activity to get the update interval
     *
     * @return the update interval in milliseconds
     */
    public static long getNewUpdateIntervalInMilliseconds(int activityType) {

        int updateIntervalInSeconds;

        switch (activityType) {
            case DetectedActivity.IN_VEHICLE:
                updateIntervalInSeconds = STANDARD_UPDATE_INTERVAL_VEHICLE_IN_SECONDS;
                break;
            case DetectedActivity.ON_BICYCLE:
                updateIntervalInSeconds = STANDARD_UPDATE_INTERVAL_BICYCLE_IN_SECONDS;
                break;
            case DetectedActivity.ON_FOOT:
                updateIntervalInSeconds = STANDARD_UPDATE_INTERVAL_WALKING_IN_SECONDS;
                break;

            // should never be in default case. New update intervals are only necessary if in motion
            default:
                throw new IllegalStateException("Illegal activityType: " + activityType + ". Has " +
                                                "to be IN_VEHICLE, ON_BICYCLE or ON_FOOT");
        }


        return TimeUnit.SECONDS.toMillis
                (updateIntervalInSeconds);

    }


    public static Long getLocationForTime(Context context, long time) {


        DaoSession daoSession = Utils.getDaoSession(context);
        SQLiteDatabase db = daoSession.getDatabase();


        String sql = "SELECT _id,  min(abs(TIME - " + time + ")) FROM SIMPLE_LOCATION";


        Cursor cursor = db.rawQuery(sql, null);

        try {


            if (cursor.moveToFirst()) {

                int idIndex = cursor.getColumnIndex(SimpleLocationDao.Properties.Id
                        .columnName);

                long id = cursor.getLong(idIndex);


                if (id > 0) {
                    return id;
                }

            }


            return null;
        } finally {
            cursor.close();
        }
    }


    /**
     * Check if the device on which the app is running has a {@link android.location
     * .LocationProvider} ({@link android.location.LocationManager#GPS_PROVIDER} or {@link
     * android.location.LocationManager#NETWORK_PROVIDER}) enabled.
     *
     * @param context Context to get SystemService for GPS
     */
    public static boolean checkIfLocationIsOn(final Context context) {

        // check if GPS is enabled
        LocationManager locationManager = (LocationManager) context.getSystemService(Context
                .LOCATION_SERVICE);
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean passive = locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);

        Log.d(TAG, "gps " + gps + "\tnetwork " + network + "\npassive " + passive);

        return gps || network;

    }

    /**
     * Show a alert where the user can turn location access on.
     *
     * @param context    Context to get dialog builder an string resources
     * @param reactivate true if the Dialog should instruct the user to reactivate position
     *                   tracking, else false
     */
    public static void showLocationAlert(final Context context, boolean reactivate) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.dialog_location_title));

        String appName = context.getString(R.string.app_name);

        // tell the user to reactivate the position tracking in the privacy module
        if (reactivate) {
            String positions = context.getString(R.string.positions);
            String privacy = context.getString(R.string.privacy);
            builder.setMessage(context.getString(R.string.dialog_location_text_reactivate_pos,
                    appName, positions, privacy));
        } else {
            builder.setMessage(context.getString(R.string.dialog_location_text,
                    appName));
        }
        builder.setCancelable(false);
        builder.setPositiveButton(context.getString(R.string.settings),
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        // open location settings to let the user turn location on
                        Intent settingsIntent = new Intent(android.provider
                                .Settings
                                .ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(settingsIntent);
                        dialog.cancel();

                    }

                }
        );
        builder.setNegativeButton(context.getString(android.R.string.cancel),
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();

                    }

                }
        );

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }


    /**
     * Return the estimated street address that is closest to the given location. If no location
     * can
     * be found, or an error occur, return null.
     *
     * @param context   Context to create {@link android.location.Geocoder} object
     * @param latitude  latitude of the point for the address
     * @param longitude longitude of the point for the address
     *
     * @return String of the nearest street address or null if an error occurred
     */
    public static String calcLocationAddress(Context context, double latitude, double longitude) {
        if (context == null || !Utils.isNetworkAvailable(context)) {
            return null;
        }

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        // Get the current location from the input parameter list
        // Create a list to contain the result address
        List<Address> addresses;
        try {
                /*
                 * Return 1 address.
                 */
            addresses = geocoder.getFromLocation(latitude, longitude,
                    1);
        } catch (IOException e1) {
            // No internet connection or something wrong with the connection
            return null;
        } catch (IllegalArgumentException e2) {
            // Error message to post in the log
            String errorString = "Illegal arguments " +
                                 latitude +
                                 " , " +
                                 longitude +
                                 " passed to address service";
            Log.e(TAG, errorString);
            return null;
        }
        // If the reverse geocode returned an address
        if (addresses != null && addresses.size() > 0) {
            // Get the first address
            Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available),
                 * city, and country name.
                 */


            String street = address.getAddressLine(0);
            // Locality is usually a  city
            String city = address.getLocality();
            String country = address.getCountryName();


            // Return the text
            return formatAddress(street, city, country);
        } else {
            return null;
        }

    }

    /**
     * Returns a formatted String of the given pieces of address
     *
     * @param street  the street, if any
     * @param city    the city, if any
     * @param country the country, if any
     *
     * @return String representation of the given pieces, or the empty String if nothing is given
     */
    public static String formatAddress(String street, String city, String country) {
        return String.format("%s%s%s",
                // If there's a street address, add it
                street != null ? street.concat(", ") : "",
                city != null ? city.concat(", ") : "",
                country != null ? country : "");
    }

    /**
     * Return the estimated street address that is closest to the given location. If no location
     * can
     * be found, or an error occur, return null.
     *
     * @param context  Context to create {@link android.location.Geocoder} object
     * @param location {@link at.ac.uibk.awarenessy.dao.SimpleLocation} to get the nearest
     *                 street address.
     *
     * @return String of the nearest street address or null if an error occurred
     */
    public static String calcLocationAddress(Context context, SimpleLocation location) {

        if (location == null) {
            return null;
        }

        return calcLocationAddress(context, location.getLatitude(), location.getLongitude());

    }

    /**
     * Get the latitude and longitude from the Location object returned by Location Services.
     *
     * @param currentLocation A Location object containing the current location
     *
     * @return The latitude and longitude of the current location, or null if no location is
     * available.
     */
    public static String getLatLng(Context context, Location currentLocation) {

        // If the location is valid
        if (currentLocation != null) {
            // Return the latitude and longitude as strings
            return context.getString(R.string.latitude_longitude, currentLocation.getLatitude(),
                    currentLocation.getLongitude());
        } else {
            // Otherwise, return the empty string
            return EMPTY_STRING;
        }

    }

    public static String getLatLngString(Context context, LatLng latLng) {

        return getLatLngString(context, latLng.latitude, latLng.longitude);

    }

    public static String getLatLngString(Context context, double latitude, double longitude) {


        // start with latitude degrees
        StringBuilder latLngString = new StringBuilder(String.valueOf(latitude));

        // append N or S or nothing if on degree 0.0
        if (latitude > 0) {
            latLngString.append(" ").append(context.getString(R.string.north));
        } else if (latitude < 0) {
            latLngString.append(" ").append(context.getString(R.string.south));
        }

        // append 2 spaces and longitude degrees
        latLngString.append(", ").append(String.valueOf(longitude));

        // append E or W or nothing if on degree 0.0
        if (longitude > 0) {
            latLngString.append(" ").append(context.getString(R.string.east));
        } else if (longitude < 0) {
            latLngString.append(" ").append(context.getString(R.string.west));
        }

        return latLngString.toString();
    }
}
