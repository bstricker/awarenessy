/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.movement.background;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import at.ac.uibk.awarenessy.app.utils.Log;

import com.google.android.gms.location.DetectedActivity;

import at.ac.uibk.awarenessy.app.movement.LocationUtils;
import at.ac.uibk.awarenessy.app.utils.Preferences;

/**
 * BroadcastReceiver that listens for BOOT_COMPLETED, ACTION_START_GPS_TRACKING and
 * ACTION_STOP_GPS_TRACKING
 * broadcasts to start or stop {@link BackgroundLocationService}.
 */
public class BackgroundMovementManager extends BroadcastReceiver {

    private static final String TAG = "BackgroundMovementManager";
    private static Intent mLocationServiceIntent;

    // The activity recognition update request object
    private static DetectionRequester mDetectionRequester;

    // The activity recognition update removal object
    private static DetectionRemover mDetectionRemover;


    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(TAG, "OnReceive from " + TAG + " Action: " + intent.getAction());
        mLocationServiceIntent = new Intent(context, BackgroundLocationService.class);

        boolean gpsTracking = isGpsTrackingEnabled(context);
        Log.d(TAG, "GPS tracking " + (gpsTracking ? " on" : " off"));


        // Make sure we are getting the right intent
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {

            // user has rebooted
            SharedPreferences sharedPreferences = context.getSharedPreferences(Preferences
                    .APP_DATA.PREF_NAME, Context.MODE_PRIVATE);
            sharedPreferences.edit().putBoolean(Preferences.PREF_FLAGS.REBOOTED, true).commit();

            if (gpsTracking) {
                startLocationService(context, intent);
                startRecognition(context);
            }
        } else if (LocationUtils.ACTION_START_GPS_TRACKING.equals(intent.getAction())) {
            if (gpsTracking) {
                startLocationService(context, intent);
                startRecognition(context);
            }
        } else if (LocationUtils.ACTION_STOP_GPS_TRACKING.equals(intent.getAction())) {
            stopLocationService(context);
            stopRecognition();
        } else if (LocationUtils.ACTION_START_LOCATION_UPDATES.equals(intent.getAction())) {
            if (gpsTracking) {
                startLocationService(context, intent);
            }
        } else if (LocationUtils.ACTION_STOP_LOCATION_UPDATES.equals(intent.getAction())) {
            stopLocationService(context);

        } else {
            Log.e(TAG, "Received unexpected intent " + intent.toString());

        }
    }

    private void stopRecognition() {


        if (mDetectionRequester != null && mDetectionRemover != null) {
            // Pass the remove request to the remover object
            mDetectionRemover.removeUpdates(mDetectionRequester.getRequestPendingIntent());
        /*
         * Cancel the PendingIntent. Even if the removal request fails, canceling the PendingIntent
         * will stop the updates.
         */
            mDetectionRequester.getRequestPendingIntent().cancel();

            Log.d(TAG, "Recognition stopped");


        }
    }

    private void startRecognition(Context context) {

        Log.d(TAG, "Start Recognition");


        // Get detection requester and remover objects
        mDetectionRequester = new DetectionRequester(context);
        mDetectionRemover = new DetectionRemover(context);

        // Pass the update request to the requester object
        mDetectionRequester.requestUpdates();

    }

    private void startLocationService(Context context, Intent intent) {

        int activityType = intent.getIntExtra(LocationUtils.EXTRA_ACTIVITY_TYPE,
                DetectedActivity.UNKNOWN);
        Log.d(TAG, "Type: " + activityType);
        mLocationServiceIntent.setAction(LocationUtils.ACTION_START_LOCATION_UPDATES);
        mLocationServiceIntent.putExtra(LocationUtils.EXTRA_ACTIVITY_TYPE, activityType);

        ComponentName service = context.startService(mLocationServiceIntent);
        if (null == service) {
            // something really wrong here
            Log.e(TAG, "Could not start service BackgroundLocationService");
        } else {
            Log.d(TAG, "BackgroundLocationService started");
        }
        Log.d(TAG, "Started context " + context);

    }

    private boolean isGpsTrackingEnabled(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(Preferences.PREF_FLAGS
                .PREF_NAME, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(Preferences.PREF_FLAGS.TRACK_POSITION, false);
    }

    private void stopLocationService(Context context) {

        mLocationServiceIntent.setAction(LocationUtils.ACTION_STOP_LOCATION_UPDATES);

        if (null != context.startService(mLocationServiceIntent)) {
            Log.d(TAG, "BackgroundLocationService stopped");
        } else {
            Log.d(TAG, "BackgroundLocationService isn't running");
        }


    }


}