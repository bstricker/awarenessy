/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.utils;

/**
 * Shared Preferences class that stores Identifier to get certain preferences.
 */
public class Preferences {



    /* --------------------------------------------------------------*/
    /* ------------------ APP APP_DATA PREFERENCES ----------------------*/
    /* --------------------------------------------------------------*/


    public class APP_DATA {

        public static final String PREF_NAME = "app_data";

        // Long - Used to store time (Long) when the last Sync has been done
        public static final String LAST_SENT_SMS_SYNC         = "last_sent_sms_sync";
        // Int - Type of the last recognized user activity (on vehicle, on bike, on foot, ...)
        public static final String KEY_PREVIOUS_ACTIVITY_TYPE = "previous_activity_type";
        /**
         * Stores the last known ip address.
         */
        public static final String KEY_LAST_KNOWN_IP_ADDRESS  = "last_known_ip_address";

        // Long - total distance travelled (in meters) tracked by the Movement Profile Module
        public static final String KEY_DISTANCE_TRAVELLED = "distance_travelled";

        // Strings - Name of the logged in user
        public static final String FB_FIRST_NAME = "fb_first_name";
        public static final String FB_LAST_NAME  = "fb_last_name";
    }

    public class PREF_FLAGS {

        public static final String PREF_NAME = "flags";

        // Boolean - Stores if the app (Launcher Activity) starts for the first time
        public static final String FIRST_START = "first_start";


        /**
         * Stores if the user already knows how to use the Navigation Drawer
         */
        public static final String DRAWER_TEACHED = "drawer_teached";

        // if device has rebooted since installation
        public static final String REBOOTED = "rebooted";

        public static final String SHOW_CHECK_APPS_INFO        = "show_check_apps_info";
        public static final String SHOW_SMS_STATISTIC_INFO     = "show_sms_statistic_info";
        public static final String SHOW_CONTACT_STATISTIC_INFO = "show_contact_statistic_info";
        public static final String SHOW_PROVIDER_INFO          = "show_provider_info";
        public static final String SHOW_DATA_USAGE_INFO        = "show_data_usage_info";
        public static final String SHOW_MOVEMENT_INFO          = "show_movement_info";
        public static final String SHOW_PRIVACY_INFO           = "show_privacy_info";

        public static final String SMS_SENT_IMPORTED = "sms_sent_imported";
        public static final String SMS_REC_IMPORTED  = "sms_rec_imported";

        public static final String VERSION_UPDATE = "version_update";

        // Settings in PrivacyFragment
        public static final String MONITOR_SMS    = "monitor_sms";
        public static final String TRACK_POSITION = "track_position";
        public static final String MONITOR_CALLS  = "monitor_calls";
    }
}




