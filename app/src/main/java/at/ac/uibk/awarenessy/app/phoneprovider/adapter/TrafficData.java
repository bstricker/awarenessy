/*
 * Copyright © 2014 Benedikt Stricker
 *
 * This file is part of Awarenessy.
 *
 * Awarenessy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Awarenessy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Awarenessy.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von Awarenessy.
 *
 * Awarenessy ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * Awarenessy wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package at.ac.uibk.awarenessy.app.phoneprovider.adapter;


import java.util.Comparator;

/**
 * Represent the used up- and download of a certain package.
 *
 * @see {@link UsedDataAdapter}
 */
public class TrafficData {

    private final String mName;
    private final int    mUid;
    private final Data   mSentData;
    private final Data   mRcvData;

    public TrafficData( int uid, String name, Data sentData, Data rcvData ) {

        mUid = uid;
        mName = name;
        mSentData = sentData;
        mRcvData = rcvData;
    }

    public String getName() {
        return mName;
    }

    public int getUid() {

        return mUid;
    }

    public Data getSentData() {

        return mSentData;
    }

    public Data getRcvData() {

        return mRcvData;
    }

    public Data getAllData() {

        // convert into same unit
        double sentDataInSentUnit = mSentData.getAmount();
        double rcvDataInSendUnit = mSentData.getUnit().convert(mRcvData.getAmount(),
                                                               mRcvData.getUnit());

        return new Data(sentDataInSentUnit + rcvDataInSendUnit, mSentData.getUnit());

    }


    /**
     * Perform comparison at both, sent and received data traffic.
     */
    public static final Comparator<TrafficData> ALL_DATA_COMPARATOR = new Comparator<TrafficData>
            () {

        @Override
        public int compare( TrafficData lhs, TrafficData rhs ) {

            // convert into same unit
            double lhsDataInLhsUnit = lhs.getAllData().getAmount();
            double rhsDataInLhsUnit = lhs.getAllData().getUnit().convert(rhs.getAllData()
                                                                                 .getAmount(),
                                                                         rhs.getAllData().getUnit
                                                                                 ()
            );

            return Double.compare(rhsDataInLhsUnit, lhsDataInLhsUnit);


        }
    };
}
